#include <iostream>
using namespace std;

class Insert {
public:
	int n;
	int* tab;
	Insert(int x) {
		n = x;
		tab = new int[n];
	}
	bool check() {
		for (int i = 0; i < n - 1; i++) if (tab[i] > tab[i + 1]) return false;
		return true;
	}
	void sort() {
		int el, i;
		for (int j = 1; j < n; j++) {
			el = tab[j];
			i = j - 1;
			while (i >= 0 && tab[i] > el) {
				tab[i + 1] = tab[i];
				i--;
			}
			tab[i + 1] = el;
		}
	}
	void output() {
		for (int i = 0; i < n; i++) cout << tab[i] << " ";
	}
};



int main() {
	int n;
	cin >> n;
	Insert obj(n);
	for (int i = 0; i < n; i++) cin >> obj.tab[i];
	obj.sort();
	obj.output();
	if (obj.check()) cout << "dobrze posortowane" << endl;
	return 0;
}